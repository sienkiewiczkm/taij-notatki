#!/bin/sh
shopt -s nullglob
for graphfile in ./graphs/*.dot; do
  filename=$(basename $graphfile)
  filename_noext=${filename%.dot}
  echo "Generating from $graphfile..."
  dot "$graphfile" -Tpng -o "images/$filename_noext.png"
done
echo "Done."
shopt -u nullglob
